exports.config = {
  // See http://brunch.io/#documentation for docs.
  files: {
    javascripts: {
      joinTo: "scripts/app.js"

      // To use a separate vendor.js bundle, specify two files path
      // http://brunch.io/docs/config#-files-
      // joinTo: {
      //   "js/app.js": /^js/,
      //   "js/vendor.js": /^(?!js)/
      // }
      //
      // To change the order of concatenation of files, explicitly mention here
      // order: {
      //   before: [
      //     "vendor/js/jquery-2.1.1.js",
      //     "vendor/js/bootstrap.min.js"
      //   ]
      // }
    },
    stylesheets: {
      joinTo: "styles/app.css"
    },
    templates: {
      joinTo: "scripts/app.js"
    }
  },

  conventions: {
    // This option sets where we should place non-css and non-js assets in.
    // By default, we set this to "/assets/static". Files in this directory
    // will be copied to `paths.public`, which is "priv/static" by default.
    assets: /^(static)/
  },

  // Phoenix paths configuration
  paths: {
    // Dependencies and current project directories to watch
    watched: ["static", "styles", "scripts"],
  },

  // Configure your plugins
  plugins: {
    babel: {
      // Do not use ES6 compiler in vendor code
      ignore: [/vendor/],
      presets: ['latest', 'react'],
    },
    sass: {
      mode: 'native',
    },
  },

  modules: {
    autoRequire: {
      "scripts/app.js": ["scripts/app"]
    }
  },

  npm: {
    enabled: true,
    styles: {
      bourbon: ['node_modules/bourbon/app/assets/stylesheets/_bourbon.scss'],
      neat: ['node_modules/bourbon-neat/app/assets/stylesheets/_neat.scss'],
      bitters: ['node_modules/bourbon-bitters/app/assets/stylesheets/_bitters.scss'],
    }
  },
};
